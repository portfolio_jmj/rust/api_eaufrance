use reqwest::{Client};
use crate::structures;
use crate::structures::chronique_piezometrique::ChroniquePiezometrique;
use crate::structures::response_chronique::ResponseChronique;
use crate::structures::response_request_analyse::ResponseRequestAnalyse;
use crate::structures::response_request_stations::ResponseRequestStations;
static DOMAIN: &str = "https://hubeau.eaufrance.fr/api";
#[allow(dead_code)]
pub async fn call_analyse() {
    let url: String = format!("{}{}", DOMAIN, "/v1/qualite_nappes/analyses");
    let client = Client::new();
    let response = client
        .get(url)
        .query(&[
            ("code_bss", "07548X0009/F"),
            ("date_debut_mesure", "2021-12-31"),
            ("date_fin_mesure", "2022-01-02"),
            ("size", "20"),
        ])
        .send();
    match response.await {
        Ok(response_result) => match response_result.status() {
            reqwest::StatusCode::OK | reqwest::StatusCode::PARTIAL_CONTENT => {
                println!("Ok status code : {}", response_result.status());
                println!("{:?}", response_result.json::<ResponseRequestAnalyse>().await)
            }
            _ => {
                println!("ok mais des 20X status code : {}", response_result.status())
            }
        },
        Err(error) => println!("{:?}", error),
    }
}

#[allow(dead_code)]
pub async fn call_stations(){
    let url: String = format!("{}{}", DOMAIN, "/v1/qualite_nappes/stations");
    let client = Client::new();
    let response = client
        .get(url)
        /*.query(&[
            ("code_bss", "07548X0009/F"),
            ("date_debut_mesure", "2021-12-31"),
            ("date_fin_mesure", "2022-01-02"),
            ("size", "20"),
        ])*/
        .send();
    match response.await {
        Ok(response_result) => match response_result.status() {
            reqwest::StatusCode::OK | reqwest::StatusCode::PARTIAL_CONTENT => {
                println!("Ok status code : {}", response_result.status());
                println!("{:?}", response_result.json::<ResponseRequestStations>().await)
            }
            _ => {
                println!("ok mais des 20X status code : {}", response_result.status())
            }
        },
        Err(error) => println!("{:?}", error),
    }
}

//Chronique 1 point par jour
#[allow(dead_code)]
pub async fn call_chronique_piezo(p_code_bss:&str) {
    let url: String = format!("{}{}", DOMAIN, "/v1/niveaux_nappes/chroniques");
    let client = Client::new();
    let response = client
        .get(url)
        .query(&[
            ("code_bss", p_code_bss),
            ("sort","desc"),
            ("fields","timestamp_mesure,niveau_nappe_eau,profondeur_nappe,date_mesure,statut,qualification")
        ])
        .send();
    match response.await {
        Ok(response_result) => match response_result.status() {
            reqwest::StatusCode::OK | reqwest::StatusCode::PARTIAL_CONTENT => {
                println!("Ok status code : {}", response_result.status());
                println!("{:?}", response_result.json::<ResponseChronique<ChroniquePiezometrique>>().await)
            }
            _ => {
                println!("ok mais des 20X status code : {}", response_result.status())
            }
        },
        Err(error) => println!("{:?}", error),
    }
}

//Chronique temps réel 1 point par heure
#[allow(dead_code)]
pub async fn call_chronique_piezo_tr(p_code_bss: &str) -> Result<structures::response_chronique::ResponseChronique<structures::chronique_piezometrique::ChroniquePiezometrique>,reqwest::Error> {
    let url: String = format!("{}{}", DOMAIN, "/v1/niveaux_nappes/chroniques_tr");
    let client = Client::new();
    let response = client
        .get(url)
        .query(&[
            ("code_bss", p_code_bss),
            ("size","1"),
            ("sort","desc"),
            ("fields","timestamp_mesure,niveau_nappe_eau,profondeur_nappe,date_mesure,statut,qualification")
        ])
        .send();
    match response.await {
        Ok(response_result) => {
                //println!("Ok status code : {}", response_result.status());
                //println!("{:?}", response_result.json::<response_chronique::ResponseChronique<chronique_piezo::ChroniquePiezometrique>>().await)
                response_result.json::<ResponseChronique<ChroniquePiezometrique>>().await
        },
        Err(error) => {
            println!("{:?}", error);
            Err(error)
        }
    }
}

//Renvoie les données en string
pub async fn call_chronique_piezo_tr_raw(p_code_bss: &str) -> String {
    let url: String = format!("{}{}", DOMAIN, "/v1/niveaux_nappes/chroniques_tr");
    let client = Client::new();
    let response = client
        .get(url)
        .query(&[
            ("code_bss", p_code_bss),
            ("size","1"),
            ("sort", "desc"),
            ("fields", "timestamp_mesure,niveau_nappe_eau,profondeur_nappe,date_mesure,statut,qualification")
        ])
        .send();
    match response.await {
        Ok(res) => res.text().await.unwrap(),
        Err(error) => error.status().unwrap().to_string()
    }
}