use std::fs;
use std::path::Path;
use crate::structures::response_chronique::ResponseChronique;
use crate::structures::station_meusure_piezometrique::StationMesurePiezometrique;

//Parse et renvoie la liste des stations
#[allow(dead_code)]
pub fn load_stations()-> ResponseChronique<StationMesurePiezometrique> {
    let json_file_path = Path::new("../datas/stations.json");
    let data = fs::read_to_string(json_file_path).expect("Unable to read file");
    let niveaux_stations:ResponseChronique<StationMesurePiezometrique> = serde_json::from_str(&data)
        .expect("error while reading or parsing");

    return niveaux_stations;
}

pub fn load_stations_raw()-> String {
    let json_file_path = Path::new("../datas/stations.json");
    let data = fs::read_to_string(json_file_path).expect("Unable to read file");
    return data;
}
