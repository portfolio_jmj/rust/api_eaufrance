use serde::Deserialize;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct ResponseChronique<T>{
    count:i32,
    first:Option<String>,
    last:Option<String>,
    prev:Option<String>,
    next:Option<String>,
    api_version:Option<String>,
    data:Vec<T>
}