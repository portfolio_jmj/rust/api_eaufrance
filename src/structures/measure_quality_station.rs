use serde::Deserialize;
use crate::structures::point::Point;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct MeasureQualityStation {
    #[serde(rename = "codeBassinDce")]
    code_bassin_dce:Option<String>,
    bss_id:Option<String>,
    code_bss:Option<String>,
    urn_bss:Option<String>,
    date_debut_mesure: Option<String>,
    date_fin_mesure: Option<String>,
    precision_coordonnees:Option<i32>,
    longitude:Option<f64>,
    latitude:Option<f64>,
    altitude:Option<String>,
    code_insee:Option<String>,
    nom_commune:Option<String>,
    num_departement:Option<String>,
    nom_departement:Option<String>,
    nom_region:Option<String>,
    circonscriptions_administrative_bassin:Option<Vec<Option<String>>>,
    bassin_dce:Option<String>,
    urn_bassin_dce:Option<String>,
    code_nature_pe:Option<i32>,
    nom_nature_pe:Option<String>,
    uri_nature_pe:Option<String>,
    libelle_pe:Option<String>,
    code_caracteristique_aquifere:Option<String>,
    nom_caracteristique_aquifere:Option<String>,
    uri_caracteristique_aquifere:Option<String>,
    code_etat_pe:Option<i32>,
    nom_etat_pe:Option<String>,
    uri_etat_pe:Option<String>,
    code_mode_gisement:Option<i32>,
    nom_mode_gisement:Option<String>,
    uri_mode_gisement:Option<String>,
    profondeur_investigation:Option<f32>,
    commentaire_pe:Option<String>,
    codes_entite_hg_bdlisa:Option<Vec<Option<String>>> ,
    noms_entite_hg_bdlisa:Option<Vec<Option<String>>>,
    urns_bdlisa:Option<Vec<Option<String>>>,
    codes_masse_eau_rap:Option<Vec<Option<String>>>,
    noms_masse_eau_rap:Option<Vec<Option<String>>>,
    urns_masse_eau_rap:Option<Vec<Option<String>>>,
    codes_masse_eau_edl:Option<Vec<Option<String>>>,
    noms_masse_eau_edl:Option<Vec<Option<String>>>,
    urns_masse_eau_edl:Option<Vec<Option<String>>>,
    codes_reseau:Option<Vec<Option<String>>>,
    noms_reseau:Option<Vec<Option<String>>>,
    uris_reseau:Option<Vec<Option<String>>>,
    geometry: Point
}