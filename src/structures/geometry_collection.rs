use serde::Deserialize;
use crate::structures::crs::Crs;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct GeometryCollection {
    bbox:Vec<f64>,
    crs:Crs,
    //geometries
}