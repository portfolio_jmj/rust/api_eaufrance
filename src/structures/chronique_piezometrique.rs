use serde::Deserialize;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct ChroniquePiezometrique {
    code_continuite: Option<String>,
    code_nature_mesure:Option<String>,
    code_producteur:Option<String>,
    nom_continuite:Option<String>,
    nom_nature_mesure:Option<String>,
    nom_producteur:Option<String>,
    profondeur_nappe:Option<f64>,
    //Mandatory : Code de la station (code BSS)
    code_bss:Option<String>,
    //Mandatory : URN de la station
    urn_bss:Option<String>,
    date_mesure:Option<String>,
    timestamp_mesure:Option<i64>,
    //Mètre
    niveau_nappe_eau:Option<f32>,
    mode_obtention:Option<String>,
    //http://id.eaufrance.fr/nsa/415
    statut:Option<String>,
    //http://id.eaufrance.fr/nsa/414
    qualification:Option<String>,

}