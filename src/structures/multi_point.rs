use serde::Deserialize;
use crate::structures::crs::Crs;
use crate::structures::lng_lat_alt::LngLatAlt;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct MultiPoint {
    bbox:Vec<f64>,
    coordinates:Vec<LngLatAlt>,
    crs:Crs
}