use serde::Deserialize;
use crate::structures::crs::Crs;
use crate::structures::lng_lat_alt::LngLatAlt;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct LineString {
    bbox:Vec<f64>,
    coordinates:LngLatAlt,
    crs:Crs
}