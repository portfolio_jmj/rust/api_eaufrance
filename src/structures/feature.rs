use serde::Deserialize;
use crate::structures::crs::Crs;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct Feature{
    bbox:Vec<f32>,
    crs:Crs,
    //geometry
    id:String,
    //properties
}