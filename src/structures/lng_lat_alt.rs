use serde::Deserialize;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct LngLatAlt{
    #[serde(rename = "additionalElements")]
    additional_elements: Vec<String>,
    altitude:String,
    latitude:String,
    longitude:String
}