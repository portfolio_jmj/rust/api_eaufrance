use serde::Deserialize;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct ChroniquePiezometriqueTempsReel {
    date_maj: Option<String>,
    //http://infoterre.brgm.fr/nouveau-code-bss
    bss_id:Option<String>,
    //Mandatory : Code de la station (code BSS)
    code_bss:Option<String>,
    //Mandatory : URN de la station
    urn_bss:Option<String>,
    //Mandatory
    longitude:Option<String>,
    //Mandatory
    latitude:Option<String>,
    //Mandatory
    altitude_station:Option<f64>,
    //Mandatory
    altitude_repere:Option<f64>,
    date_mesure:Option<String>,
    timestamp_mesure:Option<String>,
    profondeur_nappe:Option<f32>,
    niveau_eau_ngf:Option<f32>
}