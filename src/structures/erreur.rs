use serde::Deserialize;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct Erreur{
    code:String,
    message:String,
    field:String
}