use serde::Deserialize;
use crate::structures::measure_quality_station::MeasureQualityStation;
#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct ResponseRequestStations {
    count:i32,
    first:String,
    last:Option<String>,
    prev:Option<String>,
    next:Option<String>,
    api_version:String,
    data:Vec<MeasureQualityStation>
}