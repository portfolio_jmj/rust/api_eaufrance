use serde::Deserialize;
#[path = "point.rs"] mod point;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct StationMesurePiezometrique {
    //Mandatory
    code_bss:Option<String>,
    //Mandatory
    urn_bss:Option<String>,
    date_debut_mesure:Option<String>,
    date_fin_mesure:Option<String>,
    code_commune_insee:Option<String>,
    nom_commune:Option<String>,
    x:Option<f64>,
    y:Option<f64>,
    codes_bdlisa:Option<Vec<Option<String>>>,
    urns_bdlisa:Option<Vec<Option<String>>>,
    geometry:Option<point::Point>,
    bss_id:Option<String>,
    altitude_station:Option<String>,
    nb_mesures_piezo:Option<i32>,
    code_departement:Option<String>,
    nom_departement:Option<String>,
    libelle_pe:Option<String>,
    profondeur_investigation:Option<f32>,
    codes_masse_eau_edl:Option<Vec<Option<String>>>,
    noms_masse_eau_edl:Option<Vec<Option<String>>>,
    urns_masse_eau_edl:Option<Vec<Option<String>>>,
    date_maj:Option<String>

}