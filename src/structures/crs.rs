use serde::Deserialize;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct Crs{
    //properties:
    r#type:String
}

#[derive(Debug,Deserialize)]
#[allow(unused)]
enum EnumType {
    Name(String),
    Link(String)
}