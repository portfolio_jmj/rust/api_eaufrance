use serde::Deserialize;
use crate::structures::crs::Crs;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct Point{
    //bbox:Vec<String>,
    coordinates:Option<Vec<f64>>,//lng_lat_alt::LngLatAlt,
    crs:Option<Crs>
}