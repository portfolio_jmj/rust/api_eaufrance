use serde::Deserialize;
use crate::structures::erreur::Erreur;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct MessageErreur {
    code:String,
    message:String,
    field_errors:Vec<Erreur>
}