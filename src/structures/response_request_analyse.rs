use serde::Deserialize;
use crate::structures::analyze_quality;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct ResponseRequestAnalyse{
    count:i32,
    first:String,
    last:Option<String>,
    prev:Option<String>,
    next:Option<String>,
    api_version:String,
    data:Vec<analyze_quality::AnalyzeQuality>
}