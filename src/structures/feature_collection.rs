use serde::Deserialize;
use crate::structures::crs::Crs;
use crate::structures::feature::Feature;

#[derive(Debug,Deserialize)]
#[allow(unused)]
pub struct FeatureCollection {
    bbox:Vec<f64>,
    crs:Crs,
    features:Vec<Feature>
}