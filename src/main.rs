use actix_cors::Cors;
use actix_web::{App, HttpServer, Responder, get, HttpResponse, http::header::ContentType, web};
mod services;
mod structures;


#[get("/")]
async fn live() -> impl Responder {
    //HttpResponse::Ok().body("Hello!")
    format!("Hello")
}

#[get("/json")]
async fn hello_json() -> impl Responder {
    HttpResponse::Ok()
        .content_type(ContentType::json())
        .body("{\"message\": \"Hello!\"}")
}

#[get("/chronique_piezo_tr/{station}")]
async fn get_chronique_piezo_tr(station: web::Path<String>) -> impl Responder {
    //let _s = "09906X0161/PZ1";
    let ss = station.replace("*","/");
    let v_response = services::api_v1::call_chronique_piezo_tr_raw(&ss).await;
    HttpResponse::Ok()
        .content_type(ContentType::json())
        .body(v_response)
}

#[get("/station")]
async fn station_all() -> impl Responder {
    HttpResponse::Ok()
        .content_type(ContentType::json())
        .body(services::data::load_stations_raw())
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    //println!("Serveur API de l'eau");
    //aPI_v1::call_analyse().await;
    //api_v1::call_stations().await;
    //api_v1::call_chronique_piezo("09906X0161/PZ1").await;
    //api_v1::call_chronique_piezo_tr("09906X0161/PZ1").await;
    //data_service::load_niveau_stations();

    HttpServer::new(|| {
        let cors = Cors::permissive();
        App::new()
            .service(live)
            .service(hello_json)
            .service(station_all)
            .service(get_chronique_piezo_tr)
            .wrap(cors)
    })
        .bind(("localhost", 2023))?
        .run()
        .await.unwrap();

    Ok(())
}
