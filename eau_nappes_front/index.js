const langage = document.getElementsByTagName("html")[0].getAttribute("lang");
console.log("langage", langage)

async function getStationList() {
    const res = await fetch("http://localhost:2023/station", { mode: "cors" });
    const s = await res.json();
    return s.data;
}

async function getStationData(station) {
    const res = await fetch(`http://localhost:2023/chronique_piezo_tr/${station}`, { mode: "cors" });
    const s = await res.json();
    return s.data;
}

let stationList = [];
let stationListMaker = [];

//y puis x
var map = L.map('map').setView([43.582565071, 3.747110697], 13);
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

//map.addEventListener("zoomend",handleMoveOrZoomEnd) // est déjà triger par le moveend
map.addEventListener("moveend",handleMoveOrZoomEnd)

let points = map.getBounds();
let topLeft = [points._northEast.lat, points._northEast.lng];
let bottomRight = [points._southWest.lat, points._southWest.lng];

getStationList().then((data) => {
    stationList = data
    createMarker();
}
);

function createMarker() {
    for (let station of stationList) {
        if (station.y == null || station.x == null )
            continue;

        if(stationListMaker.findIndex(item => item == station.code_bss) != -1)
        continue;

        const lat = station.y;
        const lng = station.x;

        if(!pointIn(lat,lng))
            continue;

        getStationData(station.code_bss.replace("/", "*")).then((stationData) => {
            if (stationData == null || stationData.length == 0)
                return;

            L.marker([station.y, station.x])
                .bindPopup(
                    `${fmtStationNature(station.noms_masse_eau_edl)}
                <a href="${station.urn_bss}" target="_blank">lien d'information</a>
                <legend> niveau : ${stationData[0].profondeur_nappe} mètres au ${fmTimestampIntl(stationData[0].timestamp_mesure)}  </legend>
                `)
                .addTo(map);

            stationListMaker.push(station.code_bss);
        })

    };

    console.log("map",map)
}

function pointIn(lat, lng) {
    if (lat >= Math.min(topLeft[0], bottomRight[0]) && lat <= Math.max(topLeft[0], bottomRight[0])) {
        if (lng >= Math.min(topLeft[1], bottomRight[1]) && lng <= Math.max(topLeft[1], bottomRight[1])) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function handleMoveOrZoomEnd(e){
    console.log(e)
    points = map.getBounds();
    topLeft = [points._northEast.lat, points._northEast.lng];
    bottomRight = [points._southWest.lat, points._southWest.lng];
    createMarker();
}

function fmtStationNature(noms_masse_eau_edl) {
    if (noms_masse_eau_edl == null || noms_masse_eau_edl == undefined)
        return;
    let message = "\t";
    if (noms_masse_eau_edl != null && noms_masse_eau_edl.length > 0)
        message += "<b>Natures de la nappe : </b><br>"
    else
        message += "<b>Natures de la nappe : </b><br>"

    noms_masse_eau_edl.forEach(nom => {
        message += nom + "<br>"
    });
    return message;
}

function fmtDate(date) {
    const ds = date.split("-").join(",");
    const d = new Date(Date(ds));

    return new Intl.DateTimeFormat(langage).format(d);
}

function fmTimestampIntl(timestamp) {

    return new Intl.DateTimeFormat(langage, { day: "2-digit", month: "2-digit", year: "numeric", hour: "2-digit" }).format(new Date(timestamp));
}